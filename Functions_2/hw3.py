from math import gcd

def count_points(first_point, second_point, third_point):
    x1, y1 = first_point
    x2, y2 = second_point
    x3, y3 = third_point

    boundary_points = gcd(abs(x1 - x2), abs(y1 - y2)) + \
                      gcd(abs(x2 - x3), abs(y2 - y3)) + \
                      gcd(abs(x3 - x1), abs(y3 - y1))

    triangle_area = abs((x2 - x1) * (y3 - y1) - (x3 - x1) * (y2 - y1)) / 2
    interior_points = triangle_area - (boundary_points / 2) + 1
    return int(interior_points + boundary_points)


print(count_points((0, 0), (1, 0), (0, 1)))
print(count_points((0, 0), (2, 0), (0, 2)))
print(count_points((0, 0), (-3, 0), (0, -3)))
print(count_points((0, 0), (-4, 0), (0, -4)))
