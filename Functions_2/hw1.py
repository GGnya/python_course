def operation(number):
    if number % 2 == 0:
        return number // 2
    else:
        return number * 3 + 1


def counting_the_number(string_, number=''):
    if not string_:
        return operation(int(number))

    if string_[0].isdecimal():
        number = number + string_[0]
        return counting_the_number(string_[1:], number)
    else:
        return 'Не удалось преобразовать введенный текст в число.'


def receiving_text():
    a = input()
    if a == 'cancel':
        print('bye')
        return
    elif not a:
        print('Введите что-нибудь')
    else:
        print(counting_the_number(a))
    receiving_text()


if __name__ == '__main__':
    receiving_text()
