from threading import Timer


def make_cache(save_time=3):
    cache_dict = {}

    def inner(func):

        def wrapper(*args):
            if args in cache_dict:
                return cache_dict[args]
            else:
                result = func(*args)
            save_data(args, result)
            Timer(save_time, delete_data, [args]).start()
            return result

        return wrapper

    def delete_data(key):
        if key in cache_dict:
            cache_dict.pop(key)
        return cache_dict

    def save_data(args, data):
        cache_dict.update({args: data})

    return inner


@make_cache(5)
def fib(n):
    if n < 2: return n
    return fib(n - 1) + fib(n - 2)


for i in range(100):
    print(fib(i))
