import time

time_counter = [0, 0]

def timer(func):
    def wrapper(*args, **kwargs):
        global time_counter
        first_time = time.perf_counter()
        result = func(*args, **kwargs)
        second_time = time.perf_counter()
        time_counter[0] += second_time - first_time
        time_counter[1] += 1
        return result
    return wrapper


@timer
def fibonacci(n):
    if n in (1, 2):
        return 1
    return fibonacci(n - 1) + fibonacci(n - 2)


print(fibonacci(10))
print(time_counter)
