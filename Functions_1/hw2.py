"""Напишите реализацию функции make_it_count, которая принимает в качестве
аргументов некую функцию (обозначим ее func) и имя глобальной переменной
(обозначим её counter_name), возвращая новую функцию, которая ведет себя
в точности как функция func, за тем исключением, что всякий раз при вызове
инкрементирует значение глобальной переменной с именем counter_name.
"""

counter_name = 0

def make_it_count(function, *args, **kwargs):
    global counter_name
    counter_name += 1
    print(f'counter_name = {counter_name}')
    return function(*args, **kwargs)


print(make_it_count(sum, [2, 3, 4, 5, 6]))
print(make_it_count(round, 5.5))

print(make_it_count(sum, [2, 7]))
print(make_it_count(sum, [2, 21]))
print(make_it_count(sum, [2, -3]))
print(make_it_count(sum, [2, 3]))
