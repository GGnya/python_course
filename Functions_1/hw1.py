"""
Напишите функцию partial, которая принимает функцию (обозначим ее func),
а также произвольный набор позиционных (назовем их fixated_args) и именованных
(назовем их fixated_kwargs) аргументов и возвращет новую функцию,
которая обладает следующими свойствами:
1.При вызове без аргументов повторяет поведение функции func, вызванной
с fixated_args и fixated_kwargs.
2.При вызове с позиционными и именованными аргументами дополняет ими
fixated_args (приписывает в конец списка fixated_args), и fixated_kwargs
(приписывает новые именованные аргументы и переопределяет значения старых)
и далее повторяет поведение func с этим новым набором аргументов.
3.Имеет __name__ вида partial_<имя функции func>
4.Имеет docstring вида:
    A func implementation of <имя функции func>
    with pre-applied arguments being:
    <перечисление имен и значений переданных fixated_args и fixated_kwargs;
    если ключевые аргументы переданны как позиционные - соотнести их с именами
    из function definition
    >
    source_code:
       ...
Пояснение.
partial - удобный способ получать новые функции, реализующие ограниченную
или специфическую функциональность других функций.
Например, мы хотим округлять числа с помощью функции round, но
нас интересует округление всегда только до двух знаков после запятой, поэтому
мы могли бы сделать так:
round = partial(round, ndigits=2)
И теперь round всегда округляет числа так, как нам надо и нам не нужно
постоянно писать в коде выражения вроде round(n, ndigits=2).
Конечно в Python уже есть реализация partial, однако в рамках курса,
мы просим вас сделать собственную реализацию =).
Для того, чтобы получить имена позиционных аргументов и исходный код, советую
использовать возможности модуля inspect.
Попробуйте применить эту функцию на написанных функциях из дз1, дз2, дз3.
К функциям min, max, any() ?
"""

import textwrap

def partial(func, *args, **kwargs):

    def newfunc(*fixated_args, **fixated_kwargs):
        newkwargs = {**kwargs, **fixated_kwargs}
        newfunc.__name__ = f'{func.__name__}'
        newfunc.__doc__ = textwrap.dedent(f'''\
        A partial implementation of {newfunc.__name__}
        with pre-applied arguments being:
        {args + fixated_args} {newkwargs}''')
        return func(*(args + fixated_args), **newkwargs)

    newfunc.func = func
    newfunc.args = args
    newfunc.kwargs = kwargs

    return newfunc

round = partial(round, ndigits=2)
print(round(5.24789, ndigits=3))
print(round.__name__)
print(round.__doc__)
