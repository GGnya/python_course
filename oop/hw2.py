class AbstractOptimiser:

    operation_dict = {'^': 3, '*': 2, '/': 2, '+': 1, '-': 1}

    def process(self, graph):
        g = self.pre_process(graph)

        result = self.process_internal(g)

        return self.post_process(result)

    def pre_process(self, graph):

        buffer_string = ''
        graph_list = []

        for item in graph:
            if item.isdecimal():
                buffer_string += item
            elif item.isalpha():
                buffer_string += item
            else:
                if buffer_string:
                    graph_list.append(buffer_string)
                    buffer_string = ''
                graph_list.append(item)

        if buffer_string:
            graph_list.append(buffer_string)

        return graph_list

    def process_internal(self, graph):
        return graph

    def post_process(self, result):
        return result


class DoubleNegativeOptimiser(AbstractOptimiser):

    def process_internal(self, graph):

        final_final_list = []
        final_list = []

        for index, item in enumerate(graph):
            if item == '-' and graph[index + 1] == '(' and graph[index + 2] == '-':
                counter_lit = 0
                for i in graph[index + 1:]:
                    if i.isdecimal() or i.isalpha():
                        counter_lit += 1
                    elif i == ')':
                        break
                if counter_lit <= 1:
                    graph[index] = ''
                    graph[index + 1] = '('
                    graph[index + 2] = ''

        for item in graph:
            if item:
                final_list.append(item)

        for index, item in enumerate(final_list):
            final_final_list.append(item)
            if index + 1 < len(final_list):
                if (item.isalpha() or item.isdecimal()) and final_list[index + 1] == '(':
                    final_final_list.append('+')
                elif item == ')' and (final_list[index + 1] == '(' or final_list[index + 1].isdecimal() or final_list[index + 1].isalpha()):
                    final_final_list.append('+')

        return final_final_list


class IntegerCostantsOptimiser(AbstractOptimiser):

    def pre_process(self, graph):
        return graph

    def process_internal(self, graph):
        return graph



class SimplifierOptimiser(AbstractOptimiser):

    def pre_process(self, expr):
        index_list = []
        num_brack = 0
        expr_list = []
        counter_bracket = 0
        buffer_str = ''
        for index, item in enumerate(expr):
            if item != '(' and item != ')' and item not in self.operation_dict:
                buffer_str += item
            elif item == '-' and expr[index - 1] == '(':
                buffer_str += item
            if item == '(':
                counter_bracket += 1
                num_brack += 3
            elif item == ')':
                num_brack -= 3
                counter_bracket += 1
            if item in self.operation_dict and expr[index-1] != '(':
                expr_list.append(buffer_str)
                buffer_str = ''
                expr_list.append(item)
                buffer = num_brack + self.operation_dict[item]
                index_list.append([index - counter_bracket, buffer])

        if buffer_str:
            expr_list.append(buffer_str)

        index_list = sorted(index_list, key=lambda x: -x[1])

        return [index_list, expr_list]

    def process_internal(self, mylist):
        index_list, expr_list = mylist

        for counter, item in enumerate(index_list):

            if expr_list[item[0]] in self.operation_dict:
                counter1 = 1
                counter2 = 1
                first_item = expr_list[item[0] - counter1]

                while not first_item:
                    counter1 += 1
                    first_item = expr_list[item[0] - counter1]

                second_item = expr_list[item[0] + 1]
                while not second_item:
                    counter2 += 1
                    second_item = expr_list[item[0] + counter2]

                if expr_list[item[0]] == '+':
                    result = Variable(first_item) + Variable(second_item)

                elif expr_list[item[0]] == '-':
                    result = Variable(first_item) - Variable(second_item)

                elif expr_list[item[0]] == '*':
                    result = Variable(first_item) * Variable(second_item)

                elif expr_list[item[0]] == '/':
                    result = Variable(first_item) / Variable(second_item)

                elif expr_list[item[0]] == '^':
                    result = Variable(first_item) ** Variable(second_item)

                expr_list[item[0] - counter1] = ''
                expr_list[item[0] + counter2] = ''

                expr_list[item[0]] = result

        return result


class Variable():
    def __init__(self, item):
        self.item = item

    def __add__(self, other):

        if self.item == '0' and other.item == '0':
            return '0'
        elif self.item == '0':
            return other.item
        elif other.item == '0':
            return self.item

        elif self.item.isdecimal() and other.item.isdecimal():
            return f'{int(self.item) + int(other.item)}'
        elif self.item == other.item:
            if self.item[0] == '-':
                return f'2*({self.item})'
            else:
                return f'2*{self.item}'
        elif other.item[0] == '-' and self.item == other.item[1:]:
            return '0'
        elif self.item[0] == '-' and self.item[1:] == other.item:
            return '0'
        else:
            return f'({self.item}+{other.item})'

    def __sub__(self, other):

        if self.item.isdecimal() and other.item.isdecimal():
            return f'{int(self.item) - int(other.item)}'
        elif self.item == other.item:
            return '0'
        else:
            return f'{self.item}-{other.item}'

    def __mul__(self, other):

        if self.item == '0' or other.item == '0':
            return '0'
        elif self.item == '1':
            return other.item
        elif other.item == '1':
            return self.item
        elif self.item.isdecimal() and other.item.isdecimal():
            return f'{int(self.item) * int(other.item)}'

        elif not self.item.isdecimal() and other.item.isdecimal():
            return f'({other.item}*{self.item})'

        elif self.item.isdecimal() and not other.item.isdecimal():
            return f'({self.item}*{other.item})'

        elif self.item == other.item:
            if len(self.item) == 1:
                return f'({self.item})^2'
            else:
                return f'({self.item})^2'
        else:
            return f'({self.item}*{other.item})'

    def __truediv__(self, other):
        if self.item.isdecimal() and other.item.isdecimal():
            return f'{int(self.item) // int(other.item)}'
        elif not self.item.isdecimal() and other.item.isdecimal():
            return f'({self.item}/{other.item})'
        elif self.item.isdecimal() and not other.item.isdecimal():
            return f'({self.item}/{other.item})'
        elif self.item == other.item:
            return '1'
        else:
            return f'{self.item}/{other.item}'

    def __pow__(self, other):
        if self.item == '0':
            return '0'
        elif other.item == '0':
            return '1'
        elif other.item == '1':
            return self.item
        elif self.item.isdecimal() and other.item.isdecimal():
            return f'{int(self.item) ** int(other.item)}'
        else:
            return f'{self.item ^ other.item}'

    def __eq__(self, other):
        if str(self.item) == str(other.item):
            return True
        else:
            return False
