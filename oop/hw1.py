'''Реализовать:
1) доработать конструктор (возможно) и метод validate у класса Calculator. Метод validate должен говорить, является ли выражение переданное в конструкторе вычислимым, что означает:
  1.1) у выражения правильный баланс скобок
  1.2) можно построить граф вычисления выражения
  1.2) выражение не содержит запрещённые операции (в нашем случае это только деление на 0)
2) магический метод __str__ у класса Calculator

Проверки:


1)
validate_check_list = [
	('a+2', True),
	('a-(-2)', True),
	('a+2-', False),
	('a+(2+(3+5)', False),
	('a^2', True),
	('a^(-2), True),
	('-a-2', True),
	('6/0', False),
	('a/(b-b), True), # мы же не знаем, что b-b это 0, увы
]

for case, exp in validate_check_list:
    tokens = list(case)

    calc = Calculator(tokens).validate()

	if calc != exp:
		print(f'Error in case for "{case}". Actual "{calc}", expected {exp}')

2)
str_check_list = [
	("a", "a"),
	("-a", "a-"),
	("(a*(b/c)+((d-f)/k))", "abc/*df-k/+"),
	("(a)", "a"),
	("a*(b+c)", "abc+*"),
	("(a*(b/c)+((d-f)/k))*(h*(g-r))", "abc/*df-k/+hgr-**"),
	("(x*y)/(j*z)+g", "xy*jz*/g+"),
	("a-(b+c)", "abc+-"),
	("a/(b+c)", "abc+/"),
	("a^(b+c)", "abc+^"),
]

for case, exp in str_check_list:
    tokens = list(case)
    calc = Calculator(tokens)

	if str(calc) != exp:
		print(f'Error in case for "{case}". Actual "{calc}", expected {exp}')

'''
from hw2 import DoubleNegativeOptimiser, IntegerCostantsOptimiser, SimplifierOptimiser

class Calculator:
    symbols = '^*/-+'

    def __init__(self, opcodes: list, operators: list = None):
        self.opcodes = opcodes
        self.operators = operators if operators is not None else []

    def optimise(self):
        for operator in self.operators:
            self.opcodes = operator.process(self.opcodes)



    def validate(self):
        bracket = 0
        for index, item in enumerate(self.opcodes):
            if item == '(':
                bracket += 1
                if not self.opcodes[index + 1].isalpha() and not self.opcodes[index + 1].isdigit() \
                        and self.opcodes[index + 1] != '-':
                    # проверка что после скобки стоит буква, цифра или минус
                    return False
                elif index != 0 and self.opcodes[index - 1] not in self.symbols:
                    # проверка если перед скобкой стоит знак
                    return False

            if item == ')':
                bracket -= 1
                if bracket < 0:
                    # проверка количества закрывающих скобок
                    return False
                if self.opcodes[index - 1] in self.symbols and self.opcodes[index - 1] == '(':
                    # проверка что перед скобкой стоит  скобка или знак
                    return False
                elif len(self.opcodes) != index + 1 and self.opcodes[index + 1] not in self.symbols:
                    # проверка что что после скобки  стоит символ
                    return False

            if item.isalpha():
                if index != 0:
                    if self.opcodes[index - 1] not in self.symbols and self.opcodes[index - 1] != '(':
                        # проверка что перед буквой  символ или скобка
                        return False
                if index + 1 != len(self.opcodes):
                    if self.opcodes[index + 1] not in self.symbols and self.opcodes[index + 1] != ')':
                        # проверка что после буквы стоит символ или закрывающая скобка
                        return False

            if item.isdecimal():
                if index + 1 != len(self.opcodes):
                    if self.opcodes[index + 1] not in self.symbols and not self.opcodes[index + 1].isdecimal() \
                            and self.opcodes[index + 1] != ')':
                        # проверка что после цифры идет символ, цифра или закрывающая скобка
                        return False
                if index != 0:
                    if self.opcodes[index - 1] not in self.symbols and not self.opcodes[index - 1].isdecimal() \
                            and self.opcodes[index - 1] != '(':
                        # проверка что перед цифрой идет символ, цифра или открывающая скобка
                        return False

            if item in self.symbols:
                if index + 1 == len(self.opcodes):
                    # символ не мождет быть последним
                    return False

                elif index == 0 and item != '-':
                    # символ не может быть первым, если это не "-"
                    return False

                elif not self.opcodes[index + 1].isdecimal() and not self.opcodes[index + 1].isalpha() \
                        and self.opcodes[index + 1] != '(':
                    # справа от символа  цифра,  буква или  открывающая скобка
                    return False

                elif not self.opcodes[index - 1].isdecimal() and not self.opcodes[index - 1].isalpha() \
                        and self.opcodes[index - 1] != ')' and item != '-':
                    # слева от символа  цифра,  буква или закрывающая скобка и это не минус
                    return False

                if item == '/' and self.opcodes[index + 1] == '0':
                    # проверка деления на ноль
                    return False

        return bracket == 0

    def __str__(self) -> str:
        priority = {'^': 3, '*': 2, '/': 2, '+': 1, '-': 1}
        final_string = ''
        buffer_list = []
        for item in self.opcodes:
            if item.isalpha() or item.isdecimal():
                final_string += item
            elif item == '(':
                buffer_list += item
            elif item == ')':
                while buffer_list[len(buffer_list) - 1] != '(':
                    final_string += buffer_list.pop()
                else:
                    buffer_list.pop()

            elif item in self.symbols:
                if buffer_list and buffer_list[-1] != '(':
                    if priority[item] <= priority[buffer_list[-1]]:
                        final_string += buffer_list.pop()
                buffer_list += item

        # Какой из методов лучше цикл или джоин?
        # if buffer_list:
        #     for item in buffer_list:
        #         final_string += item

        if buffer_list:
            final_string += ''.join(buffer_list)

        return final_string


validate_check_list = [
    ('a+2', True),
    ('a-(-2)', True),
    ('a+2-', False),
    ('a+(2+(3+5)', False),
    ('a^2', True),
    ('a^(-2)', True),
    ('-a-2', True),
    ('6/0', False),
    ('a/(b-b)', True),  # мы же не знаем, что b-b это 0, увы
    ('a/(b+b)b', False),
    ('()+a+b', False),
    ('a--b', False),
    ('a-c(-b)', False),
    ('+a+c+b', False),
    ('--a+b', False),
    ('a+(-b-c-)', False),
    ('-a-b-', False),
    ('a-+b', False),
    ('a-(--b)', False),
]

for case, exp in validate_check_list:
    tokens = list(case)
    calc = Calculator(tokens).validate()
    if calc != exp:
        print(f'Error in case for "{case}". Actual "{calc}", expected {exp}')

str_check_list = [
    ("a", "a"),
    ("-a", "a-"),
    ("(a*(b/c)+((d-f)/k))", "abc/*df-k/+"),
    ("(a)", "a"),
    ("a*(b+c)", "abc+*"),
    ("(a*(b/c)+((d-f)/k))*(h*(g-r))", "abc/*df-k/+hgr-**"),
    ("(x*y)/(j*z)+g", "xy*jz*/g+"),
    ("a-(b+c)", "abc+-"),
    ("a/(b+c)", "abc+/"),
    ("a^(b+c)", "abc+^"),
    ("1^(2+3)", "123+^"),
    ('-(-a-b)', 'a-b--')
]

for case, exp in str_check_list:
    tokens = list(case)
    calc = Calculator(tokens)
    str(calc)
    if str(calc) != exp:
        print(f'Error in case for "{case}". Actual "{calc}", expected {exp}')

double_negate_tests = [
    ('-(-var)', 'var'),
    ('-(-5)', '5'),
    ('-(a+b)+c-(-d)', 'ab+-c+d+'),
    ('5-(-(-5))', '55-+'),
    ('a+(-b)', 'ab-+'),
    ('a+(-(-b))', 'ab+'),
    ('-(a+b)+c-(-d)+(-e)-(-f)-(-(-g))', 'ab+-c+d+e-+f+g-+'),
    ('-(a+b)+c-(-d)+(-e)-(-f)-(-(-(-g)))', 'ab+-c+d+e-+f+g+'),
    ('-(-a)+b', 'ab+'),
    ('-(-a+b)+c', 'a-b+-c+'),
    ('-(-a+b)', 'a-b+-'),
    ('-(-(a+b)+c)', 'ab+-c+-')
]

for case, exp in double_negate_tests:
    tokens = list(case)
    calc = Calculator(tokens, [DoubleNegativeOptimiser()])
    calc.optimise()

    if str(calc) != exp:
        print(f'Error in case for "{case}". Actual "{calc}", expected {exp}')

# integer_constant_optimiser_tests = [
#     (['1'], ['1']),
#     (['1', '+', '2'], ['3']),
#     (['1', '-', '2'], ['1-']),
#     (['2', '*', '2'], ['4']),
#     (['2', '/', '2'], ['1']),
#     (['2', '^', '10'], ['1024']),
#     (['a', '+', '2', '*', '4'], ['a8+', '8a+']),
#     (['a', '+', 'b', '+', 'c', '+', '11', '-', '10'], ['ab+c+1+']),
#     (['a+7-6-1'], ['a0+']),
#     (['15', '*', 'a', '/', '5'], ['a3*']),
#     (['2', '+', 'a', '-', 'b', '*', '3', '/', 'c'], ['']),
#
#     (['2', '+', 'a', '+', '3'], ['5a+', 'a5+']),  # (*)
# ]
#
# for case, exp in integer_constant_optimiser_tests:
#     calc = Calculator(case, [DoubleNegativeOptimiser(), IntegerCostantsOptimiser()])
#
#     calc.optimise()
#
#     if str(calc) not in exp:
#         print(f'Error in case for "{case}". Actual "{calc}", expected one of {exp}')

simplifier_optimiser_test = [
    ('a+0', ['a']),
    ('a*1', ['a']),
    ('a*0', ['0']),
    ('b/b', ['1']),
    ('a-a', ['0']),
    ('a+(b-b)', ['a']),
    ('a+(7-6-1)', ['a']),
    ('a^0', ['1']),
    ('a-(-(-a))', ['0']),
    ('a-(-a)', ['2a*']),

    ('a+a+a', ['a3*', '3a*']),  # (*)
    ('(a-b)-(a-b)', ['0']),  # (*)
    ('(a-b)/(a-b)', ['1']),  # (*)
    ('(a+b)+(a+b)', ['ab+2*', '2ab+*']),  # (*)
    ('a*b+a*b', ['2ab**', '2ba**', 'a2b**', 'ab2**', 'b2a**', 'ba2**']),  # (*)
]

for case, exps in simplifier_optimiser_test:
    tokens = list(case)
    calc = Calculator(tokens, [DoubleNegativeOptimiser(), IntegerCostantsOptimiser(), SimplifierOptimiser()])

    calc.optimise()

    if str(calc) not in exps:
        print(f'Error in case for "{case}". Actual "{calc}", expected one of {exps}')
