# !/usr/bin/env python
# -*- coding: utf-8 -*-
import time
import numpy as np
import pandas as pd
import math
import matplotlib.pyplot as plt


def timer(func):
    def wrapper(*args):
        t_start_time = time.perf_counter()
        func(*args)
        t_stop_time = time.perf_counter()
        return t_stop_time - t_start_time
    return wrapper


@timer
def multer(a, b):
    np.dot(a, b)


def matrix_creator(column_row):

    return np.random.randint(1, 10, (column_row, column_row)), np.random.randint(1, 10, (column_row, column_row))


def matrix_size_time_yielder():
    for i in range(1, 301):
        time_list = [i, []]
        for _ in range(30):
            a, b = matrix_creator(i)
            time_list[1].append(multer(a, b))

        yield time_list


def standard_deviation_calc(a, avg):
    sum_ = 0

    for item in a:
        sum_ += (item - avg)**2

    return (sum_/len(a))**0.5


def average_time(dataframe):
    average_list = []
    for item in dataframe['time']:
        avg = math.fsum(item)/len(item)
        average_list.append(avg)
    return average_list


df = pd.DataFrame(list(matrix_size_time_yielder()), columns=['matrix_size', 'time'], index=None)
avg_time = np.array(average_time(df), dtype='float64')
df.insert(2, 'avg_time', avg_time)

standard_deviation = []

for index, _ in enumerate(df['matrix_size']):
    standard_deviation.append(standard_deviation_calc(df['time'][index], df['avg_time'][index]))

df.insert(3, 'standard_deviation', standard_deviation)

df['logarithm_average_time'] = np.log10(df['avg_time'])
df['logarithm_matrix_size'] = np.log10(df['matrix_size'])

def logarithmer_sd(df_):
    logarithm_sd_list = []
    for index, avg_time in enumerate(df_['avg_time']):
        logarithm_sd = np.log10(np.absolute(avg_time)) - np.log10(np.absolute(avg_time - df_['standard_deviation'][index]))
        logarithm_sd_list.append(logarithm_sd)
    return logarithm_sd_list


df.insert(4, 'logarithm_sd', logarithmer_sd(df))



def calc_n(first_time, second_time, first_matrix_size, second_matrix_size):

    return math.fabs((math.log10(math.fabs(second_time / first_time))) / (math.log10(math.fabs(second_matrix_size / first_matrix_size))))


def n_lister(df_):
    n_list = []
    n_avg_list_ = []

    for index, _ in enumerate(df_['matrix_size']):
        n_list.append([])
        a = 9
        if a == index:
            a = 11
        second_time = df_['avg_time'][a]
        second_matrix_size = df_['matrix_size'][a]

        first_matrix_size = df_['matrix_size'][index]
        n_avg_list_.append(calc_n(df_['avg_time'][index], second_time, first_matrix_size, second_matrix_size))

        for first_time in df_['time'][index]:
            n_list[index].append(calc_n(first_time, second_time, first_matrix_size, second_matrix_size))

    n_deviation_list_ = []

    for index, item in enumerate(n_list):
        n_deviation_list_.append(standard_deviation_calc(item, n_avg_list_[index]))

    return n_avg_list_, n_deviation_list_


n_avg_list, n_deviation_list = n_lister(df)

df.insert(5, 'n_avg_list', n_avg_list)
df.insert(6, 'n_deviation_list', n_deviation_list)

def draw_graph(df_):

    fig, (ax1, ax2) = plt.subplots(1, 2)
    fig.suptitle('Графики зависимости времени от размера матриц при умножении')


    ax1.errorbar(df_['logarithm_matrix_size'], df_['logarithm_average_time'],
                 yerr=df_['logarithm_sd'],
                 fmt='-o', color='seagreen', capsize=2,
                 label='avg_time')

    ax1.set_xlabel('log(Размер матрицы)')
    ax1.set_ylabel('log(Времени)')
    ax1.legend()
    ax1.grid()

    ax2.errorbar(df_['matrix_size'], df_['n_avg_list'],
                 yerr=df_['n_deviation_list'],
                 fmt='-ro', capsize=2,
                 label='n = log(T2/T1)/log(N2/N1)')
    ax2.set_xlabel('Размер матрицы')
    ax2.set_ylabel('Сложность алгоритма')
    ax2.legend()
    ax2.grid()

    fig.set_figheight(5)
    fig.set_figwidth(11)
    plt.show()

    fig.savefig('мой график.png')
    fig.savefig('мой график.svg')
    fig.savefig('мой график.eps')


draw_graph(df)
