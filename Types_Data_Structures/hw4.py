import sys
while True:
    my_str = input('Введите строку:')
    if my_str == 'cancel':
        sys.exit()
    my_set = set(my_str.split(' '))
    int_set = set([int(x) for x in my_set])
    number_set = set(list(range(1, len(my_set))))
    print(min(int_set ^ number_set))