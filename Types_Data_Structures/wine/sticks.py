from datetime import datetime
import psycopg2


def generator(str):
    buffer = ''

    for i in str:
        if i == '{':
            buffer = ''
        elif i == '}':
            yield buffer
        else:
            buffer += i


def dictator(mystr):
    mystr += ','
    buffer = ''
    word_flag = False
    mydict = {}

    for item in mystr:
        if buffer == ' ':
            buffer = ''
        if item == '"':
            if word_flag:
                word_flag = False
            elif not word_flag:
                word_flag = True
        elif item == ',' and not word_flag:
            value = buffer
            if value == 'null' and key == 'price':
                value = 0
            elif value.isdigit():
                value = int(value)
            buffer = ''
            mydict.update({key: value})
        elif item == ':' and not word_flag:
            key = buffer
            buffer = ''
        else:
            buffer += item

    return mydict


def average_price(_list, variety=None):
    _full_price = 0
    counter = 0

    for item in _list:
        if variety is None and item['price'] != 0:
            _full_price += item['price']
            counter += 1
        elif item['variety'] == variety and item['price'] != 0:
            _full_price += item['price']
            counter += 1
    if counter != 0:
        _average_price = _full_price//counter

        return _average_price


def min_price(_list, variety):
    _min_price = average_price(_list, variety)

    for item in _list:
        if item['variety'] == variety:
            if item['price'] < _min_price and item['price'] != 0:
                _min_price = item['price']

    return _min_price


def max_price(_list, variety):
    _max_price = average_price(_list, variety)

    for item in _list:
        if item['variety'] == variety:
            if item['price'] > _max_price:
                _max_price = item['price']

    return _max_price


def most_common_region(_list, variety):
    most_common_region_dict = {}
    most_common_region_list = []
    max_value = 0

    for item in _list:
        if item['variety'] == variety:
            region_1 = item['region_1']
            region_2 = item['region_2']

            if region_1 != 'null':
                if region_1 in most_common_region_dict:
                    most_common_region_dict[region_1] += 1
                elif region_1 not in most_common_region_dict:
                    most_common_region_dict.update({region_1: 1})
            elif region_1 == 'null' and region_2 != 'null':
                if region_2 in most_common_region_dict:
                    most_common_region_dict[region_2] += 1
                elif region_2 not in most_common_region_dict:
                    most_common_region_dict.update({region_2: 1})

    for key, value in most_common_region_dict.items():
        if value > max_value:
            most_common_region_list = []
            max_value = value
            most_common_region_list.append(key)
            most_common_region_list.append(value)
        elif value == max_value:
            most_common_region_list.append(key)
            most_common_region_list.append(value)

    return most_common_region_list


def most_common_country(_list, variety):
    most_common_country_dict = {}
    max_value = 0
    most_common_country_list = []

    for item in _list:
        if item['variety'] == variety:
            if item['country'] in most_common_country_dict:
                most_common_country_dict[item['country']] += 1
            elif item['country'] not in most_common_country_dict:
                most_common_country_dict.update({item['country']: 1})

    for key, value in most_common_country_dict.items():
        if value > max_value:
            most_common_country_list = []
            max_value = value
            most_common_country_list.append(key)
            most_common_country_list.append(value)
        elif value == max_value:
            most_common_country_list.append(key)
            most_common_country_list.append(value)

    return most_common_country_list


def average_score(_list, variety=None):
    _full_score = 0
    counter = 0

    for item in _list:
        if variety is None and item['points'] != 0:
            _full_score += item['points']
            counter += 1
        elif item['variety'] == variety and item['points'] != 0:
            _full_score += item['points']
            counter += 1
    if counter != 0:
        _average_score = _full_score//counter
        return _average_score


def most_expensive_wine(_list):
    final_list = []
    max_price = 0

    for item in _list:
        buffer_list = []
        buffer_list.append(item['title'])
        buffer_list.append(item['price'])
        if item['price'] == max_price:
            final_list.append(buffer_list)
        elif item['price'] > max_price:
            max_price = item['price']
            final_list = []
            final_list.append(buffer_list)

    return final_list


def cheapest_wine(_list):
    min_price = average_price(_list)
    final_list = []

    for item in _list:
        buffer_list = []
        buffer_list.append(item['title'])
        buffer_list.append(item['price'])
        if item['price'] == min_price:
            final_list.append(buffer_list)
        elif item['price'] < min_price and item['price'] != 0:
            min_price = item['price']
            final_list = []
            final_list.append(buffer_list)

    return final_list


def highest_score(_list):
    final_list = []
    max_score = 0

    for item in _list:
        buffer_list = []
        buffer_list.append(item['title'])
        buffer_list.append(item['points'])
        if item['points'] == max_score:
            final_list.append(buffer_list)
        elif item['points'] > max_score:
            max_score = item['points']
            final_list = []
            final_list.append(buffer_list)

    return final_list


def lowest_score(_list):
    final_list = []
    min_score = average_score(_list)

    for item in _list:
        buffer_list = []
        buffer_list.append(item['title'])
        buffer_list.append(item['points'])
        if item['points'] == min_score:
            final_list.append(buffer_list)
        elif item['points'] < min_score:
            min_score = item['points']
            final_list = []
            final_list.append(buffer_list)

    return final_list


def country_lister(_list, purpose):
    country_list = []
    buffer_dict = {}

    for item in _list:
        country = item['country']
        value = item[purpose]
        if country not in buffer_dict and value != 0:
            buffer_dict.update({country: [value, 1]})
        elif country in buffer_dict and value != 0:
            buffer_dict[country][0] += value
            buffer_dict[country][1] += 1

    for key, value in buffer_dict.items():
        country_list.append([key, value[0]//value[1]])

    return country_list


def most_expensive_country(_list):
    max_price = 0
    price_list = country_lister(_list, 'price')

    for item in price_list:
        if item[0] == 'null':
            continue
        if item[1] > max_price:
            max_price = item[1]
            max_price_list = []
            max_price_list.append(item)
        elif item[1] == max_price:
            max_price_list.append(item)

    return max_price_list


def cheapest_country(_list):
    price_list = country_lister(_list, 'price')
    min_price = price_list[0][1]
    min_price_list = []

    for item in price_list:
        if item[0] == 'null':
            continue
        if item[1] < min_price:
            min_price = item[1]
            min_price_list = []
            min_price_list.append(item)
        elif item[1] == min_price:
            min_price_list.append(item)

    return min_price_list


def most_rated_country(_list):
    max_rate = 0
    rate_list = country_lister(_list, 'points')

    for item in rate_list:
        if item[0] == 'null':
            continue
        if item[1] > max_rate:
            max_rate = item[1]
            max_rate_list = []
            max_rate_list.append(item)
        elif item[1] == max_price:
            max_rate_list.append(item)

    return max_rate_list


def underrated_country(_list):
    rate_list = country_lister(_list, 'points')
    min_rate = rate_list[0][1]
    min_rate_list = []

    for item in rate_list:
        if item[0] == 'null':
            continue
        if item[1] < min_rate:
            min_rate = item[1]
            min_rate_list = []
            min_rate_list.append(item)
        elif item[1] == min_rate:
            min_rate_list.append(item)

    return min_rate_list


def most_active_commentator(_list):
    commentator_dict = {}
    commentator_list = []
    commentator_value = 0

    for item in _list:
        if item['taster_name'] == 'null':
            continue
        if item['taster_name'] not in commentator_dict:
            commentator_dict.update({item['taster_name']: 1})
        else:
            commentator_dict[item['taster_name']] += 1

    for key, value in commentator_dict.items():
        if value > commentator_value:
            commentator_list = []
            commentator_list.append([key, value])
            commentator_value = value
        elif value == commentator_value:
            commentator_list.append([key, value])

    return commentator_list


def average_information(_list):
    stat_dict = {}

    stat_dict.update({'most_expensive_wine': most_expensive_wine(_list)})
    stat_dict.update({'cheapes_wine': cheapest_wine(_list)})
    stat_dict.update({'highest_score': highest_score(_list)})
    stat_dict.update({'lowest_score': lowest_score(_list)})
    stat_dict.update({'most_expensive_country': most_expensive_country(_list)})
    stat_dict.update({'cheapest_country': cheapest_country(_list)})
    stat_dict.update({'most_rated_country': most_rated_country(_list)})
    stat_dict.update({'underrated_country': underrated_country(_list)})
    stat_dict.update({'most_active_taster': most_active_commentator(_list)})

    return stat_dict


def average_information_variety(_list, *variety):
    name_stat_dict = {}

    for item in variety:
        stat_dict = {}
        stat_dict.update({'average_price': average_price(_list, item)})
        stat_dict.update({'min_price': min_price(_list, item)})
        stat_dict.update({'max_price': max_price(_list, item)})
        stat_dict.update({'most_common_region':
                           most_common_region(_list, item)})
        stat_dict.update({'most_common_country':
                           most_common_country(_list, item)})
        stat_dict.update({'average_price': average_score(_list, item)})
        if stat_dict:
            name_stat_dict.update({item: stat_dict})

    return name_stat_dict


start_time = datetime.now()

with open('winedata_2.json') as winedata_2:
    x = winedata_2.read()
    y = x.encode().decode('unicode-escape')
    gene = generator(y)
    union_set = set()
    for i in gene:
        union_set.add(i)

with open('winedata_1.json') as winedata_1:
    x = winedata_1.read()
    y = x.encode().decode('unicode-escape')
    gene = generator(y)
    for i in gene:
        union_set.add(i)


union_list = []

for item in union_set:
    union_list.append(dictator(item))

sorted_list = sorted(union_list, key=lambda p: (-p['price'], p['variety']))

with open('full_winedata.json', 'w', encoding='utf-8') as full_winedata:
    full_winedata.write(", ".join(map(str, [sorted_list])))

wine_dict = average_information_variety(union_list,
                        'Gewürztraminer', 'Riesling', 'Merlot', 'Madera',
                        'Tempranillo', 'Red Blend')

stat_string = '{"statistics": {\n\t\t"wine": {'

for key, value in wine_dict.items():
    stat_string += f'\n\t\t\t"{key}": {value},'

stat_string +='\n\t\t},'

for key, value in average_information(union_list).items():
    stat_string += f'\n\t\t"{key}": {value},'

stat_string +='\n\t}\n}'

with open('stats.json', 'w', encoding='utf-8') as stats:
    stats.write(stat_string)


# Создание таблицы и добавление данных в database, не расскомменчивать

# conn = psycopg2.connect(user='postgres', database='wine_db', password='1234', host='127.0.0.1', port='5432')
# cur = conn.cursor()
# conn.autocommit = True
#
# cur.execute("""CREATE TABLE IF NOT EXISTS wine(
#     wine_id SERIAL PRIMARY KEY,
#     points INTEGER,
#     title TEXT,
#     description TEXT,
#     taster_name TEXT,
#     taster_twitter_handle TEXT,
#     price INTEGER,
#     designation TEXT,
#     variety TEXT,
#     region_1 TEXT,
#     region_2 TEXT,
#     province TEXT,
#     country TEXT,
#     winery TEXT);
# """)
#
# insert_smth = '''INSERT INTO wine (points, title, description, taster_name, taster_twitter_handle, price, designation, variety, region_1, region_2, province, country, winery)
#                  VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)'''
#
# for item in union_list:
#     data = tuple(item.values())
#     cur.execute(insert_smth, data)
#
# conn.commit()
# conn.close()

end_time = datetime.now()
print(end_time-start_time)
