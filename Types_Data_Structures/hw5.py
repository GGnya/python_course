def is_palindrom(start1=1, end1=1000001):
    summ = 0
    for index, number in enumerate(range(start1, end1)):
        byte_number = str(bin(number))[2::]
        if index % 100000 == 0:
            print(f'Проверено {index} чисел')
        if str(number) == str(number)[::-1] and byte_number == byte_number[::-1]:
            summ += number

    print(f'Сумма всех палиндромов {summ}')

if __name__ == '__main__':
    is_palindrom()