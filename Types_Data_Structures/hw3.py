def line_cleaner(my_str):
    final_list = []
    my_str_int = ''
    my_str += 'f'
    for index, item in enumerate(my_str):
        if item.isdecimal():
            my_str_int += item
        elif item == '-' and my_str[index + 1].isdecimal():
            my_str_int = ''
            my_str_int += item
        else:
            if my_str_int != '':
                final_list.append(my_str_int)
                my_str_int = ''
    return summ(final_list)

def summ(my_str):
    summ = 0
    for item in my_str:
        summ += int(item)
    return summ

while True:
    my_str = input('> ')
    print(line_cleaner(my_str))