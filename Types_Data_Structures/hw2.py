import sys

while True:
    my_str = input("Введите строку>\n").lower()
    my_dict = {}
    count = 1
    if my_str == 'cancel':
        sys.exit()

    for item in my_str.split():
        if not item in my_dict:
            my_dict.update({item: count})
        else:
            count = my_dict.get(item)
            count += 1
            my_dict.update({item: count})

    for key, value in my_dict.items():
        if value == max(my_dict.values()):
            print(key, value)
