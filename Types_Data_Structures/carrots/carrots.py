"""
Задание 1
0) Повторение понятий из биологии (ДНК, РНК, нуклеотид, протеин, кодон)
1) Построение статистики по входящим в последовательность ДНК нуклеотидам
для каждого гена (например: [A - 46, C - 66, G - 23, T - 34])
2) Перевод последовательности ДНК в РНК (окей, Гугл)
3) Перевод последовательности РНК в протеин*
*В папке files вы найдете файл rna_codon_table.txt -
в нем содержится таблица переводов кодонов РНК в аминокислоту,
составляющую часть полипептидной цепи белка.
Вход: файл dna.fasta с n-количеством генов
Выход - 3 файла:
 - статистика по количеству нуклеотидов в ДНК
 - последовательность РНК для каждого гена
 - последовательность кодонов для каждого гена
 ** Если вы умеете в matplotlib/seaborn или еще что,
 welcome за дополнительными баллами за
 гистограммы по нуклеотидной статистике.
 (Не забудьте подписать оси)
P.S. За незакрытый файловый дескриптор - караем штрафным дезе.
"""

from collections import Counter

def count_nucleotides(dna):
    num_of_nucleotides = {}
    dna_dict = {}
    dna_dict_value = ''
    dna_dict_key = ''

    for key, value in dna.items():
        new_value = [k for k in Counter(value).items()]
        new_value.sort()
        num_of_nucleotides.update({key: new_value})

    return num_of_nucleotides

def translate_from_dna_to_rna(dna):
    rna = {}

    for key, value in dna.items():
        value = value.replace('T', 'U')
        rna.update({key: value})
        rna.update({key: [value[i:i+3] for i in range(0, len(value), 3)]})

    return rna


def dictate(dna):
    """Разбиение входящей строки на словарь"""
    dna_dict = {}
    dna_dict_value = ''
    dna_dict_key = ''

    for line in dna:
        if line.startswith('>'):
            dna_dict_key = line.replace('\n', '').replace('>', '')
            dna_dict_value = ''
        else:
            dna_dict_value += line.replace('\n', '')
        dna_dict.update({dna_dict_key: dna_dict_value})

    return dna_dict

def translate_rna_to_protein(rna, codon_dict):
    proteins_str = ''
    proteins = {}

    for key, value in rna.items():
        proteins_str += ''.join([codon_dict[item] for item in value if len(item) == 3])
        proteins.update({key: proteins_str})
        proteins_str = ''

    return proteins

def restruct_codon_table(rna_codon_table):
    """Перевод страницы с рнк в словарь"""
    codon_dict = {}
    codon_str = ''
    codon_list = []

    for item in rna_codon_table.read():
        codon_str += item

    codon_str = codon_str.replace('p', 'p ').replace('  ', '*').replace('\n', '*')
    codon_list =  codon_str.split('*')
    codon_list = [value for value in codon_list if value != '']

    for item in codon_list:
        codon_dict.update({item[0:3]: item[4:]})

    return codon_dict

def main():

    with open('dna.fasta') as dna:
        x = count_nucleotides(dictate(dna))

    with open('stat_count_nucleotides.txt', 'w') as stat_count_nucleotides:
        for key, value in x.items():
            stat_count_nucleotides.write(f'{key}: {value}\n')

    with open('dna.fasta') as dna:
        y = translate_from_dna_to_rna(dictate(dna))

    with open('translation.txt', 'w') as translation:
        for key, value in y.items():
            translation.write(f'{key}: {value}\n')

    with open('rna_codon_table.txt') as codon_table:
        z = translate_rna_to_protein(y, restruct_codon_table(codon_table))

    with open('proteins.txt', 'w') as proteins:
        for key, value in z.items():
            proteins.write(f'{key}: {value}\n')


if __name__ == '__main__':
    try:
        main()
        print('Операция выполнена удачно')
    except:
        print('Миша все хуйня, переделывай')
