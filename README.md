# python_course

Use this repository to get and store educational materials,
such as slide for lectures and homeworks

First time, enter 
*git clone https://gitlab.com/epam_python_course/python_course.git*

to get a copy of repository as a folder.

Subsequently do *git pull* inside your cloned repository folder to update it 
and thus get fresh materials when they appear.